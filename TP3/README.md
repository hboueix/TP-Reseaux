# TP3 : Routage INTER-VLAN + mise en situation


## I. Router-on-a-stick

- 🌞 Prove me that your setup is actually working

    - think about VLANs, ping, etc.

On a donc cette infra :
```
                +-----+
                |     |
                |  R1 |
                +-+---+
                  | f0/0
                  |                    +-----+
                  |                e0  |     |
                  |              +-----+  PC4|
                  | e1/0     e0/2|     +-----+
            e0/1+-+---+      +-----+
   +------------+     +------+     | e0/1
e0 |            | IOU1|      | IOU2+------+
+--+--+         +-----+      +-----+      |
|     |          |e0/2         |e0/3      | e0
|  PC1|          |             |        +-+---+
+-----+          |             |        |     |
              e0 |             |e0      | P1  |
            +----++          +-----+    +-----+
            |     |          |     |
            | PC2 |          | PC3 |
            +-----+          +-----+
```

On peut vérifier les routes du routeur :

```
R1#show ip rout
Codes: C - connected, S - static, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2
       i - IS-IS, su - IS-IS summary, L1 - IS-IS level-1, L2 - IS-IS level-2
       ia - IS-IS inter area, * - candidate default, U - per-user static route
       o - ODR, P - periodic downloaded static route

Gateway of last resort is not set

     10.0.0.0/24 is subnetted, 4 subnets
C       10.3.10.0 is directly connected, FastEthernet0/0.10
C       10.3.30.0 is directly connected, FastEthernet0/0.30
C       10.3.20.0 is directly connected, FastEthernet0/0.20
C       10.3.40.0 is directly connected, FastEthernet0/0.40
```

Et les switchs :

```
IOU1#sh interfaces trunk

Port        Mode             Encapsulation  Status        Native vlan
Et0/0       on               802.1q         trunking      1
Et1/0       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Et0/0       20,30,40
Et1/0       20,30,40

Port        Vlans allowed and active in management domain
Et0/0       20,30,40
Et1/0       20,30,40

Port        Vlans in spanning tree forwarding state and not pruned
Et0/0       20,30,40
Et1/0       20,30,40


IOU2#sh interfaces trunk

Port        Mode             Encapsulation  Status        Native vlan
Et0/0       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Et0/0       20,30,40

Port        Vlans allowed and active in management domain
Et0/0       20,30,40

Port        Vlans in spanning tree forwarding state and not pruned
Et0/0       20,30,40
```

## II. Cas concret

**Nombre de machines :**  
  
Machine | Nombre
--- | ---
Admin | 3
User | 16
Trainee | 8
Server | 4
SS | 2
Printer | 5  
  
Pour simplifier l'infra, on ne va pas mettre toutes les machines (U3 représentera tous les users de R3, S5 tous les stagiaires de R5, ...) :
![Infra GNS3](./img/infra.png)

**Réseaux utilisés :**  
  
Réseau | Adresse | VLAN | Description
--- | --- | --- | --- |
net-admin | 10.3.10.0/24 | 10 | Administrateurs
net-user | 10.3.20.0/24 | 20 | Utilisateurs
net-trainee | 10.3.30.0/24 | 30 | Stagiaires
net-srv | 10.3.40.0/24 | 40 | Serveurs
net-ss | 10.3.50.0/24 | 50 | Serveurs sensibles
net-printer | 10.3.60.0/24 | 60 | Imprimantes

**Plan d'adressage :**  
  
Machine | VLAN | `net1` | `net2` | `net3` | `net4` | `net5` | `net6`
--- | --- | --- | --- | --- | --- | --- | --- 
A1 | 10 | 10.3.10.10 | x | x | x | x | x 
A4 | 10 | 10.3.10.40 | x | x | x | x | x
A5 | 10 | 10.3.10.50 | x | x | x | x | x
U3 | 20 | x | 10.3.20.30 | x | x | x | x
U4 | 20 | x | 10.3.20.40 | x | x | x | x
U5 | 20 | x | 10.3.20.50 | x | x | x | x
S3 | 30 | x | x | 10.3.30.30 | x | x | x
S4 | 30 | x | x | 10.3.30.40 | x | x | x
S5 | 30 | x | x | 10.3.30.50 | x | x | x
SRV2 | 40 | x | x | x | 10.3.40.20 | x | x
SS2 | 50 | x | x | x | x | 10.3.50.20 | x 
P1 | 60 | x | x | x | x | x | 10.3.60.10
P3 | 60 | x | x | x | x | x | 10.3.60.30
P4 | 60 | x | x | x | x | x | 10.3.60.40
P5 | 60 | x | x | x | x | x | 10.3.60.50

Pour le router :
```
Router#show ip int br
Interface                  IP-Address      OK? Method Status                Protocol
Ethernet0/0                unassigned      YES unset  up                    up
Ethernet0/0.10             10.3.10.254     YES manual up                    up
Ethernet0/0.20             10.3.20.254     YES manual up                    up
Ethernet0/0.30             10.3.30.254     YES manual up                    up
Ethernet0/0.40             10.3.40.254     YES manual up                    up
Ethernet0/0.50             10.3.50.254     YES manual up                    up
Ethernet0/0.60             10.3.60.254     YES manual up                    up
Ethernet0/1                unassigned      YES unset  administratively down down
Ethernet0/2                unassigned      YES unset  administratively down down
...
Ethernet3/3                192.168.122.95  YES DHCP   up                    up

Router#show ip int br
Interface                  IP-Address      OK? Method Status                Protocol
Ethernet0/0                unassigned      YES NVRAM  up                    up
Ethernet0/0.10             10.3.10.254     YES NVRAM  up                    up
Ethernet0/0.20             10.3.20.254     YES NVRAM  up                    up
Ethernet0/0.30             10.3.30.254     YES NVRAM  up                    up
Ethernet0/0.40             10.3.40.254     YES NVRAM  up                    up
Ethernet0/0.50             10.3.50.254     YES NVRAM  up                    up
Ethernet0/0.60             10.3.60.254     YES NVRAM  up                    up
Ethernet0/1                192.168.122.45  YES DHCP   up                    up

```

Pour les switchs on va mettre toutes les interfaces reliées à une machine en mode access et toutes celles reliées à des switchs ou au routeur en mode access. Le switch principal aura donc toutes ses interfaces en trunk :
```
IOU-Main#show int tr

Port        Mode             Encapsulation  Status        Native vlan
Et0/0       on               802.1q         trunking      1
Et0/1       on               802.1q         trunking      1
Et0/2       on               802.1q         trunking      1
Et0/3       on               802.1q         trunking      1
Et1/0       on               802.1q         trunking      1
Et1/1       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Et0/0       10,20,30,40,50,60
Et0/1       10,60
Et0/2       40,50
Et0/3       10,20,60
Et1/0       10,20,30,60
Et1/1       10,20,30,60

Port        Vlans allowed and active in management domain
Et0/0       10,20,30,40,50,60
Et0/1       10,60
Et0/2       40,50
Et0/3       10,20,60
Et1/0       10,20,30,60
Et1/1       10,20,30,60

Port        Vlans in spanning tree forwarding state and not pruned
Et0/0       10,20,30,40,50,60
Et0/1       10,60
Et0/2       40,50
Et0/3       10,20,60
Et1/0       10,20,30,60
Et1/1       10,20,30,60
```

Eeeeeh désormais les pings fonctionnent entre toutes les machines. Pour ce qui est d'internet... Les echo request fonctionnent (pas de "host not reachable") mais pas de reply... J'obtiens un merveilleux "no response found" sur Wireshark ([No response ICMP](./img/pingTP3.pcapng)).  
Pourtant avec un ping depuis le router on a un reply mais la longueur de la trame de reply n'est pas la même que pour la request. Je n'ai pas trouvé pourquoi ça coince.
