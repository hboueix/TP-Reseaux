# TP4 : Buffet à volonté

## Sujet global : refresh

**Configuration de l'infra :** (`show run`)
- [R1](./R1.txt)
- [client-sw1](./client-sw1.txt)
- [client-sw2](./client-sw2.txt)
- [client-sw3](./client-sw3.txt)
- [infra-sw1](./infra-sw1.txt)

## Sujet au choix : Anonymat en ligne

**But : comment garantir ou renforcer son anonymat en ligne**  
  
### Proxy HTTP

🌞 Lancez Wireshark et observez le trafic émis lors d'un trafic utilisant un proxy HTTP, puis un proxy HTTPS.
  
Pour une connexion sur un site en http via un proxy, on remarque que l'adresse de destination devient l'adresse du proxy.  
  
Pour une connexion sur un site en https via un proxy https, même chose l'adresse de destination devient l'adresse du proxy. En revanche je remarque que les trames sont affichés en vert (voir image ci-dessous), de la même couleur que les trames http, tandis que sans le proxy les trames ssl sont censés être bleues.  
Je n'ai malheuresement pas discerner de différences dans les détails des trames.  

![Https proxy](./pic/https-proxy.png)

Mais il se trouve que le proxy utilisé utilise le port 80, donc en fait c'est certainement pour ça qu'il apparait en vert au vue des coloring rules de wireshark.

### Tor

#### Connexion au web avec Tor

🌞 Lancez Wireshark et observez le trafic émis lors d'un trafic utilisant le Tor Browser, comparé à une connexion classique.
  
Comparé à une connexion classique, l'adresse ip de destination à changer comme on pouvait s'y attendre. Il s'agit dans mon cas de l'ip du "guard node" de mon circuit tor, situé en Allemagne au passage.

#### Hidden service Tor

🌞 Lancez tcpdump sur la VM et récupérez la capture sur l'hôte afin de l'exploiter avec Wireshark  
  
Sans trop de surprise, on remarque que le traffic HTTP n'est pas dirigé vers l'IP de notre VM directement. Par contre je ne vois pas cette IP dans le circuit Tor.  
Peut-être est-ce ce fameux point de rendez-vous du client et du serveur.


### DoH/DoT

🌞 Utilisez Wireshark pour déterminer la différence entre DNS classique et DoH.

J'ai bien lu tout l'article de Lin Clark sur l'intérêt du DNS over HTTPS par rapport à un DNS classique, j'ai configuré mon Firefox pour qu'il utilise le DoH avec Cloudflare comme resolver mais je distingue mal la différence sur Wireshark, si ce n'est la query vers `mozilla.cloudflare-dns.com` qui n'apparaissait pas avec un DNS classique.  
Si j'ai bien compris, Firefox va forcer l'utilisation de Cloudflare comme resolver pour mes requêtes DNS afin d'éviter l'utilisation de resolver inconnus (et donc pas forcèment sécure) et de limiter les informations transmises aux serveurs DNS (avec notamment la QNAME minimization). De plus, toutes les requêtes DNS envoyées depuis Firefox utiliseront HTTPS pour empêcher les routers qui transmettront mes requêtes et leurs réponses de voir leurs contenus.

### Bilan

🌞 Faites un comparatif entre :

- proxy HTTP
- Tor
- Tor hidden servie
- DoH
- VPN (vu l'année passée)

Critères | Proxy HTTP | Tor | Hidden Service | DoH | VPN
--- | --- | --- | --- | --- | ---
Confidentialité de l'échange | Trafic non chiffré, certains proxy communique l'IP du client au serveur | Traffic chiffré entre les relais Tor, attention il ne l'est pas au dernier point de connexion si le site n'utilise pas SSL | Traffic chiffré de bout en bout | Traffic en HTTPS donc illisible | Traffic chiffré entre client et serveur, log conservé dans certains cas |
Quel message | Trafic HTTP | Traffic HTTP | Dépend du service en question | Traffic DNS | Tout le trafic |
Anonymat client | IP du client devient l'IP du proxy pour les connexions http sortantes | Empilement de connexions donc difficile de trouver IP client | IP du client inconnu du serveur | IP du client devient l'IP du resolver choisi | IP du client devient l'IP du serveur VPN |
Anonymat serveur | Pas d'anonymat | Pas d'anonymat | IP du serveur inconnu du client | Pas d'anonymat | Pas d'anonymat |

